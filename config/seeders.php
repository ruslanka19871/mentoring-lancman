<?php

return [
    'order' => [
        'users_count' => env('SEED_USERS_COUNT', 10),
        'max_orders_for_user' => env('SEED_MAX_ORDERS_FOR_USER', 10)
    ]
];

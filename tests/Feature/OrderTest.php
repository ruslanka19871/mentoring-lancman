<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use \Illuminate\Foundation\Testing\DatabaseMigrations;

    public function testCreateOrder()
    {
        $user = User::create([
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => '111111'
        ]);

        $response = $this->actingAs($user)->post('/api/orders', [
            'amount' => 100,
            'comment' => 'Test order'
        ]);

        $response->assertStatus(\Illuminate\Http\Response::HTTP_CREATED);

        $this->assertEquals(1, $user->orders->count());

    }
}

<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthTest extends TestCase
{
    use \Illuminate\Foundation\Testing\DatabaseMigrations;

    public function testLogin()
    {

        $email = 'john@doe.com';
        $password = '111111';
        $otherPassword = '222222';

        $user = User::create([
            'name' => 'John',
            'email' => $email,
            'password' => $password
        ]);

        $response = $this->post('/api/auth/login', [
            'email' => $email,
            'password' => $password
        ]);

        $response->assertStatus(\Illuminate\Http\Response::HTTP_OK);

        $response = $this->post('/api/auth/login', [
            'email' => $email,
            'password' => $otherPassword
        ]);

        $response->assertStatus(\Illuminate\Http\Response::HTTP_UNAUTHORIZED);
    }

    public function testSuccessfulRegistration()
    {

        $response = $this->post('/api/auth/register', [
            'name' => 'John',
            'email' => 'john@doe.com',
            'password' => '111111',
            'password_confirmation' => '111111'
        ]);

        $response->assertStatus(\Illuminate\Http\Response::HTTP_OK);

    }
}

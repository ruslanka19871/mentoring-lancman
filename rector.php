<?php

use Rector\Core\Configuration\Option;
use Rector\Core\ValueObject\PhpVersion;
use Rector\Php74\Rector\Property\TypedPropertyRector;
use Rector\Set\ValueObject\SetList;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function(ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();

    // paths to refactor; solid alternative to CLI arguments
    $parameters->set(Option::PATHS, [__DIR__ . '/app']);

    // is your PHP version different from the one your refactor to? [default: your PHP version], uses PHP_VERSION_ID format
    $parameters->set(Option::PHP_VERSION_FEATURES, PhpVersion::PHP_74);

    // auto import fully qualified class names? [default: false]
    $parameters->set(Option::AUTO_IMPORT_NAMES, true);

    // skip root namespace classes, like \DateTime or \Exception [default: true]
    $parameters->set(Option::IMPORT_SHORT_CLASSES, false);

    // skip classes used in PHP DocBlocks, like in /** @var \Some\Class */ [default: true]
    $parameters->set(Option::IMPORT_DOC_BLOCKS, false);

    // Run Rector only on changed files
    $parameters->set(Option::ENABLE_CACHE, true);

    // Path to phpstan with extensions, that PHPSTan in Rector uses to determine types
    $parameters->set(Option::PHPSTAN_FOR_RECTOR_PATH, getcwd() . '/phpstan.neon');

    $parameters->set(Option::SETS, [
        SetList::CODE_QUALITY,
        SetList::CODING_STYLE,
        SetList::DEAD_CLASSES,
        SetList::DEAD_CODE,
        SetList::DEAD_DOC_BLOCK,
        SetList::LARAVEL_50,
        SetList::LARAVEL_51,
        SetList::LARAVEL_52,
        SetList::LARAVEL_53,
        SetList::LARAVEL_54,
        SetList::LARAVEL_55,
        SetList::LARAVEL_56,
        SetList::LARAVEL_57,
        SetList::LARAVEL_58,
        SetList::LARAVEL_60,
        SetList::LARAVEL_STATIC_TO_INJECTION,
        SetList::LARAVEL_CODE_QUALITY,
        SetList::LARAVEL_ARRAY_STR_FUNCTION_TO_STATIC_CALL,
        SetList::NAMING,
        SetList::ORDER,
        //SetList::PERFORMANCE,
        SetList::PHPUNIT_40,
        SetList::PHPUNIT_50,
        SetList::PHPUNIT_60,
        SetList::PHPUNIT_70,
        SetList::PHPUNIT_75,
        SetList::PHPUNIT_80,
        SetList::PHPUNIT_90,
        SetList::PHPUNIT_91,
        SetList::PHPUNIT_CODE_QUALITY,
        SetList::PHPUNIT_EXCEPTION,
        SetList::PHPUNIT_INJECTOR,
        SetList::PHPUNIT_MOCK,
        SetList::PHPUNIT_SPECIFIC_METHOD,
        SetList::PHPUNIT_YIELD_DATA_PROVIDER,
        SetList::PHP_52,
        SetList::PHP_53,
        SetList::PHP_54,
        SetList::PHP_55,
        SetList::PHP_56,
        SetList::PHP_70,
        SetList::PHP_71,
        SetList::PHP_72,
        SetList::PHP_73,
        SetList::PHP_74,
        SetList::PHP_80,
        SetList::PRIVATIZATION,
        SetList::PSR_4,
        SetList::TYPE_DECLARATION,
        SetList::UNWRAP_COMPAT,
        SetList::EARLY_RETURN,
    ]);
};

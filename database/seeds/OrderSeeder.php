<?php

use Illuminate\Database\Seeder;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\User::class, config('seeders.order.users_count'))->create()->each(
            function($user) {

                $user->orders()->saveMany(
                    factory(\App\Models\Order::class, rand(0, config('seeders.order.max_orders_for_user')))->make()
                )
                ;

            }
        )
        ;

    }
}

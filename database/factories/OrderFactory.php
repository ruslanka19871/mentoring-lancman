<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Models\Order::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomElement(App\Models\User::pluck('id')->toArray()),
        'amount' => $faker->numberBetween(100, 1000)
    ];
});

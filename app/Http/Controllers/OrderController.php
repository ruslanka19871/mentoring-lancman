<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderShowRequest;
use App\Http\Requests\OrderStoreRequest;
use App\Http\Resources\OrderResource;
use App\Models\Order;

class OrderController extends Controller
{
    /**
     * @OA\Get(
     *     path="/api/orders",
     *     @OA\Response(
     *       response="200",
     *       description="User's orders list",
     *       @OA\JsonContent(
     *         @OA\Property(
     *           property="data",
     *           type="array",
     *           @OA\Items(ref="#/components/schemas/Order")
     *         )
     *       )
     *     ),
     *     @OA\Response(response=401, description="Unauthenticated")
     * )
     */
    public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return OrderResource::collection(auth()->user()->orders);
    }

    /**
     * @OA\Post(
     *     path="/api/orders",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/OrderStoreRequest")
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Order created",
     *       @OA\JsonContent(
     *         @OA\Property(
     *           property="data",
     *           type="object",
     *           ref="#/components/schemas/Order"
     *         )
     *       )
     *     ),
     *     @OA\Response(response=401, description="Unauthenticated")
     * )
     */
    public function store(OrderStoreRequest $request): OrderResource
    {
        $order = auth()->user()->orders()->create($request->validated());
        return new OrderResource($order);
    }

    /**
     * @OA\Get(
     *     path="/api/orders/{order}",
     *     @OA\Parameter(
     *       name="order",
     *       description="Order id",
     *       required=true,
     *       in="path",
     *       @OA\Schema(
     *         type="integer"
     *       )
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="Order info",
     *       @OA\JsonContent(
     *         @OA\Property(
     *           property="data",
     *           type="object",
     *           ref="#/components/schemas/Order"
     *         )
     *       )
     *     ),
     *     @OA\Response(response=401, description="Unauthenticated"),
     *     @OA\Response(response=403, description="Forbidden")
     * )
     */
    public function show(OrderShowRequest $request, Order $order): OrderResource
    {
        return new OrderResource($order);
    }
}

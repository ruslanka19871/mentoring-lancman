<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    /**
     * @OA\Post(
     *     path="/api/auth/login",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/LoginRequest")
     *      ),
     *     @OA\Response(response="200", description="User login"),
     *     @OA\Response(response=401, description="Unauthenticated")
     * )
     */
    public function login(LoginRequest $request): array
    {
        $credentials = $request->only('email', 'password');
        if (!($token = JWTAuth::attempt($credentials))) {
            throw new \App\Exceptions\InvalidCredentialsException();
        }
        return [
            'token' => $token
        ];
    }

    /**
     * @OA\Post(
     *     path="/api/auth/logout",
     *     @OA\Response(
     *       response="200",
     *       description="User logout",
     *       @OA\JsonContent(
     *         @OA\Property(
     *           property="user",
     *           type="object",
     *           ref="#/components/schemas/User"
     *         ),
     *         @OA\Property(
     *           property="msg",
     *           type="string"
     *         )
     *       )
     *     )
     * )
     */
    public function logout():array
    {
        $user = auth()->user();
        $this->guard()->logout();

        return [
            'user' => new UserResource($user),
            'msg'  => 'Logged out Successfully.'
        ];
    }

    /**
     * @OA\Post(
     *     path="/api/auth/register",
     *     @OA\RequestBody(
     *          required=true,
     *          @OA\JsonContent(ref="#/components/schemas/RegisterRequest")
     *     ),
     *     @OA\Response(
     *       response="200",
     *       description="User registration",
     *       @OA\JsonContent(
     *         @OA\Property(
     *           property="data",
     *           type="object",
     *           ref="#/components/schemas/User"
     *         )
     *       )
     *     ),
     *     @OA\Response(response=401, description="Unauthenticated")
     * )
     */
    public function register(RegisterRequest $request): UserResource
    {
        $user = User::create($request->validated());
        return new UserResource($user);
    }

    /**
     * @OA\Get(
     *     path="/api/auth/user",
     *     @OA\Response(
     *       response="200",
     *       description="Show user info",
     *       @OA\JsonContent(
     *         @OA\Property(
     *           property="data",
     *           type="object",
     *           ref="#/components/schemas/User"
     *         )
     *       )
     *     )
     * )
     */
    public function user()
    {
        $user = User::find(auth()->id());
        return new UserResource($user);
    }

    private function guard()
    {
        return auth()->guard();
    }
}

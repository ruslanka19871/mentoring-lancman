<?php

namespace App\Infrastructure\Schemas\Requests;

/**
 * @OA\Schema(
 *     title="OrderStoreRequest",
 *     description="Store order request",
 *     required={"amount"}
 * )
 */
class OrderStoreRequest
{

    /**
     * @OA\Property(
     *     title="Amount",
     *     type="numeric",
     *     format="float",
     *     minimum=0
     * )
     *
     * @var numeric
     */
    public $amount;

    /**
     * @OA\Property(
     *     title="Comment",
     *     type="string"
     * )
     *
     * @var string
     */
    public $comment;
}

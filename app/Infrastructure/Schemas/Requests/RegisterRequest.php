<?php

namespace App\Infrastructure\Schemas\Requests;

/**
 * @OA\Schema(
 *     title="RegisterRequest",
 *     description="Register request"
 * )
 */
class RegisterRequest
{

    /**
     * @OA\Property(
     *     title="User name",
     *     type="string"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     title="User email",
     *     example="john@doe.com",
     *     type="string",
     *     format="email"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     title="Password",
     *     type="string"
     * )
     *
     * @var string
     */
    public $password;

    /**
     * @OA\Property(
     *     title="Password confirmation",
     *     type="string"
     * )
     *
     * @var string
     */
    public $password_confirmation;
}

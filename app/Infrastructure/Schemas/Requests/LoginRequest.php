<?php

namespace App\Infrastructure\Schemas\Requests;

/**
 * @OA\Schema(
 *     title="LoginRequest",
 *     description="Login request"
 * )
 */
class LoginRequest
{
    /**
     * @OA\Property(
     *     title="Email",
     *     example="john@doe.com",
     *     type="string",
     *     format="email"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     title="Password",
     *     type="string"
     * )
     *
     * @var string
     */
    public $password;
}

<?php

namespace App\Infrastructure\Schemas\Responses;

/**
 * @OA\Schema(
 *     title="Order",
 *     description="Order info"
 * )
 */
class Order
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="Amount",
     *     type="numeric",
     *     format="float",
     *     minimum=0
     * )
     *
     * @var numeric
     */
    public $amount;

    /**
     * @OA\Property(
     *     title="User email",
     *     type="string"
     * )
     *
     * @var string
     */
    public $comment;

    /**
     * @OA\Property(
     *     property="user",
     *     type="object",
     *     ref="#/components/schemas/User"
     * )
     *
     * @var object
     */
    public $user;

    /**
     * @OA\Property(
     *     title="Created at",
     *     description="Created at",
     *     example="2021-02-01 01:02:03",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $created_at;
}

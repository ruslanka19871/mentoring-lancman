<?php

namespace App\Infrastructure\Schemas\Responses;

/**
 * @OA\Schema(
 *     title="User",
 *     description="User info"
 * )
 */
class User
{
    /**
     * @OA\Property(
     *     title="ID",
     *     description="ID",
     *     format="int64",
     *     example=1
     * )
     *
     * @var integer
     */
    private $id;

    /**
     * @OA\Property(
     *     title="User name",
     *     type="string"
     * )
     *
     * @var string
     */
    public $name;

    /**
     * @OA\Property(
     *     title="User email",
     *     example="john@doe.com",
     *     type="string",
     *     format="email"
     * )
     *
     * @var string
     */
    public $email;

    /**
     * @OA\Property(
     *     title="Registered at",
     *     description="Registered at",
     *     example="2021-02-01 01:02:03",
     *     format="datetime",
     *     type="string"
     * )
     *
     * @var \DateTime
     */
    private $registered_at;
}
